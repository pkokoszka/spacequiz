﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace SpaceQuiz.Behaviors
{
    public class TextBoxUpdatePropertyChanged : Behavior<TextBox>
    {
        private BindingExpression expression;
        protected override void OnAttached()
        {
            base.OnAttached();
            expression = AssociatedObject.GetBindingExpression(TextBox.TextProperty);
            AssociatedObject.TextChanged += OnTextChanged;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.TextChanged -= OnTextChanged;
            expression = null;
        }

        private void OnTextChanged(object sender, EventArgs args)
        {
            expression.UpdateSource();
        }
    }
}
