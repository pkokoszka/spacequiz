﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceQuiz.Common
{
    public class ViewNames
    {
        public const string WelcomeView = "Views/WelcomeView.xaml";
        public const string RegisterUserView = "Views/RegisterUserView.xaml";
        public const string PlayerProfileView = "Views/PlayerProfileView.xaml";
        public const string MarathonIntroView = "Views/MarathonIntroView.xaml";
        public const string MarathonQuestionView = "Views/MarathonQuestionView.xaml";
        public const string PlayersTableView = "Views/PlayersTableView.xaml";
        public const string AboutView = "Views/AboutView.xaml";
        public const string ContactView = "Views/ContactView.xaml";
        public const string ChallengeIntroView = "Views/ChallengeIntroView.xaml";
        public const string ChallengeListView = "Views/ChallengeListView.xaml";
        public const string ChallengeQuestionView = "Views/ChallengeQuestionView.xaml";
    }
}
