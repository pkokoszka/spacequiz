﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.ComponentModel;
using System.Collections.ObjectModel;
using SpaceQuiz.Model;


namespace SpaceQuiz.DataAccessLayer
{
    public class SpaceQuizDataContext : DataContext, ISpaceQuizDataContext
    {
        public static string DBConnectionString  = @"isostore:/SpaceQuiz.sdf";

        public SpaceQuizDataContext(string connectionString)
            : base(connectionString)
        {

        }


        public Table<QuizQuestion> QuizQuestions;
        public Table<QuizChallenge> QuizChallenges;
    }
}
