﻿using Microsoft.Phone.Data.Linq;
using SpaceQuiz.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceQuiz.DataAccessLayer
{
    public static class DbUpdatev1
    {
        private static DatabaseSchemaUpdater dbUpdater;
        private static SpaceQuizDataContext db;

        public static SpaceQuizDataContext Update(SpaceQuizDataContext _db)
        {
            db = _db;
            dbUpdater = _db.CreateDatabaseSchemaUpdater();
            UpdateToVersion1();

            return db;
        }

        private static void UpdateToVersion1()
        {

            List<QuizQuestion> toAddList = new List<QuizQuestion> { };


            toAddList.Add(new QuizQuestion { Question = "Jak nazywa się najstarsze drzewo rosnące w województwie świętokrzyskim?", FirstAnswer = "buk Bartek", SecondAnswer = "dąb Kuba", ThirdQuestion = "dąb Bartek", FourthAnswer = " dąb Zbigniew", CorrectAnswer = "dąb Bartek", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jakie miasto jest stolicą Australii?", FirstAnswer = "Perth", SecondAnswer = "Canberra", ThirdQuestion = "Sydney", FourthAnswer = "Colombia", CorrectAnswer = "Canberra", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak nazywa się wyspa z wulkanem Etna?", FirstAnswer = "Kreta", SecondAnswer = "Korsyka", ThirdQuestion = "Sycylia", FourthAnswer = "Malta", CorrectAnswer = "Sycylia", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "W jakim mieście znajduje się Wieża Eiffla?", FirstAnswer = "w Warszawie", SecondAnswer = "w Berlinie", ThirdQuestion = "w Paryżu", FourthAnswer = "w Rzymie", CorrectAnswer = "w Paryżu", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Kiedy odbył się chrzest Polski?", FirstAnswer = "966", SecondAnswer = "996", ThirdQuestion = "699", FourthAnswer = "669", CorrectAnswer = "966", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "W jakim mieście znajduje się Statua Wolności?", FirstAnswer = "w Waszyngtonie", SecondAnswer = "w Nowym Jorku", ThirdQuestion = "w Paryżu", FourthAnswer = "w San Francisco", CorrectAnswer = "w Waszyngtonie", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Które z województw ma najmniejszą powierzchnię?", FirstAnswer = "opolskie", SecondAnswer = "lubuskie", ThirdQuestion = "mazowieckie", FourthAnswer = "pomorskie", CorrectAnswer = "opolskie", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "W którym państwie żyją Hindusi?", FirstAnswer = "W Indiach", SecondAnswer = "W Polsce", ThirdQuestion = "W Kanadzie", FourthAnswer = "W Niemczech", CorrectAnswer = "W Indiach", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "?Jak nazywa się najwyższy szczyt Gór Świętokrzyskich?", FirstAnswer = "Łysica", SecondAnswer = "Ślęża", ThirdQuestion = "Śnieżka", FourthAnswer = "Wielka Kopa", CorrectAnswer = "Łysica", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Który kraj nazywany jest krajem kwitnącej wiśni?", FirstAnswer = "Japonia", SecondAnswer = "Chiny", ThirdQuestion = "Indochiny", FourthAnswer = "Australia", CorrectAnswer = "Japonia", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Ile jest oceanów?", FirstAnswer = "1", SecondAnswer = "2", ThirdQuestion = "3", FourthAnswer = "4", CorrectAnswer = "3", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Które państwo kojarzy się z tulipanami i wiatrakami?", FirstAnswer = "Holandia", SecondAnswer = "Chiny", ThirdQuestion = "Benelux", FourthAnswer = "Niemcy", CorrectAnswer = "Holandia", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak nazywają się góry oddzielające Europę od Azji?", FirstAnswer = "Ural", SecondAnswer = "Tatry", ThirdQuestion = "Bieszczady", FourthAnswer = "Kaukaz", CorrectAnswer = "Ural", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak się nazywa najwyższy szczyt Australii?", FirstAnswer = "Wielka Góra Wododziałowa", SecondAnswer = "Góra Kościuszki", ThirdQuestion = "Szczyt Kosciuszki", FourthAnswer = "Rysy", CorrectAnswer = "Góra Kościuszki", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak nazywa się najdalej na południe wysunięty punkt Afryki?", FirstAnswer = "Przylądek Hornless", SecondAnswer = "Przylądek Horn", ThirdQuestion = "Przylądek Igielny", FourthAnswer = "Przylądek Dobrej Nadziei", CorrectAnswer = "Przylądek Igielny", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Na jakim kontynencie leży pustynia Gobi?", FirstAnswer = " w Ameryce Południowej", SecondAnswer = "w Afryce", ThirdQuestion = "w Europie", FourthAnswer = "w Azji", CorrectAnswer = "w Azji", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak nazywa się najdłuższa rzeka w Europie?", FirstAnswer = "Ren", SecondAnswer = "Wisła", ThirdQuestion = "Wołga", FourthAnswer = "Don", CorrectAnswer = "Wołga", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Nazwa jeziora nad którym znajduje się Mysia Wieża, w której według legendy myszy zjadły Popiela?", FirstAnswer = "Gopło", SecondAnswer = "Jeziorak", ThirdQuestion = "Bajkał", FourthAnswer = "Lake City", CorrectAnswer = "Gopło", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Nad jaką rzeką leży Wiedeń?", FirstAnswer = "nad Donem", SecondAnswer = "nad Renem", ThirdQuestion = "nad Mozaną", FourthAnswer = "nad Dunajem", CorrectAnswer = "nad Dunajem", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Plitwickie Jeziora znajdują się na terytorium", FirstAnswer = "Grecji", SecondAnswer = "Albanii", ThirdQuestion = "Chorwacji", FourthAnswer = "Słowenii", CorrectAnswer = "Chorwacji", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Kto jest autorem piosenki Hey Jude?", FirstAnswer = "Led Zeppelin", SecondAnswer = "The Beatles ", ThirdQuestion = "AC/DC", FourthAnswer = "Republika", CorrectAnswer = "The Beatles ", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak nazywa się telefon produkowany przez amerykański koncern Apple?", FirstAnswer = "Iphone", SecondAnswer = "Ipad", ThirdQuestion = "Ijox", FourthAnswer = "Phone", CorrectAnswer = "Iphone", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Częścią którego morza jest Zatoka Botnicka?", FirstAnswer = "Azowskiego", SecondAnswer = "Atlantyku", ThirdQuestion = "Bałtyku", FourthAnswer = "Czarnego", CorrectAnswer = "Bałtyku", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak nazywa się jedyny niezamieszkały przez człowieka kontynent?", FirstAnswer = "Arktyka", SecondAnswer = "Antarktyka", ThirdQuestion = "Atlantyda", FourthAnswer = "Antarktyda", CorrectAnswer = "Antarktyda", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak nazywaja się łąki górskie w Bieszczadch?", FirstAnswer = "Połoniny", SecondAnswer = "Hale", ThirdQuestion = "Pampy", FourthAnswer = "Lubarwy", CorrectAnswer = "Połoniny", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Co to jest regiel?", FirstAnswer = "piętro roślinności w Bieszczadach", SecondAnswer = "piętro roślinności w Tatrach", ThirdQuestion = "piętro roślinności w Alpach", FourthAnswer = "piętro roślinności w Karpatach", CorrectAnswer = "piętro roślinności w Tatrach", IsComplete = false });



            db.QuizQuestions.InsertAllOnSubmit(toAddList);
            db.SubmitChanges();

            dbUpdater.DatabaseSchemaVersion = 1;
            dbUpdater.Execute();
        }



    }
}
