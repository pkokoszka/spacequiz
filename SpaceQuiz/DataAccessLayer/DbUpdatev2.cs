﻿using Microsoft.Phone.Data.Linq;
using SpaceQuiz.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceQuiz.DataAccessLayer
{
    public static class DbUpdatev2
    {
        private static DatabaseSchemaUpdater dbUpdater;
        private static SpaceQuizDataContext db;

        public static SpaceQuizDataContext Update(SpaceQuizDataContext _db)
        {
            db = _db;
            dbUpdater = _db.CreateDatabaseSchemaUpdater();
            UpdateToVersion2();

            return db;
        }

        private static void UpdateToVersion2()
        {
            //dbUpdater.AddColumn<QuizQuestion>("QuizChallengeIdenty");
            //dbUpdater.Execute();
            List<QuizQuestion> toAddList = new List<QuizQuestion> { };


            toAddList.Add(new QuizQuestion { Question = "Jak nazywa się najstarsze drzewo rosnące w województwie świętokrzyskim?", FirstAnswer = "buk Bartek", SecondAnswer = "dąb Kuba", ThirdQuestion = "dąb Bartek", FourthAnswer = " dąb Zbigniew", QuizChallengeIdenty = null, CorrectAnswer = "dąb Bartek", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jakie miasto jest stolicą Australii?", FirstAnswer = "Perth", SecondAnswer = "Canberra", ThirdQuestion = "Sydney", FourthAnswer = "Colombia", QuizChallengeIdenty = null, CorrectAnswer = "Canberra", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak nazywa się wyspa z wulkanem Etna?", FirstAnswer = "Kreta", SecondAnswer = "Korsyka", ThirdQuestion = "Sycylia", FourthAnswer = "Malta", QuizChallengeIdenty = null, CorrectAnswer = "Sycylia", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "W jakim mieście znajduje się Wieża Eiffla?", FirstAnswer = "w Warszawie", SecondAnswer = "w Berlinie", ThirdQuestion = "w Paryżu", FourthAnswer = "w Rzymie", QuizChallengeIdenty = null, CorrectAnswer = "w Paryżu", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Kiedy odbył się chrzest Polski?", FirstAnswer = "966", SecondAnswer = "996", ThirdQuestion = "699", FourthAnswer = "669", QuizChallengeIdenty = null, CorrectAnswer = "966", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "W jakim mieście znajduje się Statua Wolności?", FirstAnswer = "w Waszyngtonie", SecondAnswer = "w Nowym Jorku", ThirdQuestion = "w Paryżu", FourthAnswer = "w San Francisco", QuizChallengeIdenty = null, CorrectAnswer = "w Waszyngtonie", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Które z województw ma najmniejszą powierzchnię?", FirstAnswer = "opolskie", SecondAnswer = "lubuskie", ThirdQuestion = "mazowieckie", FourthAnswer = "pomorskie", QuizChallengeIdenty = null, CorrectAnswer = "opolskie", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "W którym państwie żyją Hindusi?", FirstAnswer = "W Indiach", SecondAnswer = "W Polsce", ThirdQuestion = "W Kanadzie", FourthAnswer = "W Niemczech", QuizChallengeIdenty = null, CorrectAnswer = "W Indiach", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "?Jak nazywa się najwyższy szczyt Gór Świętokrzyskich?", FirstAnswer = "Łysica", SecondAnswer = "Ślęża", ThirdQuestion = "Śnieżka", FourthAnswer = "Wielka Kopa", QuizChallengeIdenty = 227, CorrectAnswer = "Łysica", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Który kraj nazywany jest krajem kwitnącej wiśni?", FirstAnswer = "Japonia", SecondAnswer = "Chiny", ThirdQuestion = "Indochiny", FourthAnswer = "Australia", QuizChallengeIdenty = 227, CorrectAnswer = "Japonia", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Ile jest oceanów?", FirstAnswer = "1", SecondAnswer = "2", ThirdQuestion = "3", FourthAnswer = "4", QuizChallengeIdenty = 227, CorrectAnswer = "3", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Które państwo kojarzy się z tulipanami i wiatrakami?", FirstAnswer = "Holandia", SecondAnswer = "Chiny", ThirdQuestion = "Benelux", FourthAnswer = "Niemcy", QuizChallengeIdenty = 227, CorrectAnswer = "Holandia", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak nazywają się góry oddzielające Europę od Azji?", FirstAnswer = "Ural", SecondAnswer = "Tatry", ThirdQuestion = "Bieszczady", FourthAnswer = "Kaukaz", QuizChallengeIdenty = 227, CorrectAnswer = "Ural", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak się nazywa najwyższy szczyt Australii?", FirstAnswer = "Wielka Góra Wododziałowa", SecondAnswer = "Góra Kościuszki", ThirdQuestion = "Szczyt Kosciuszki", FourthAnswer = "Rysy", QuizChallengeIdenty = 227, CorrectAnswer = "Góra Kościuszki", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak nazywa się najdalej na południe wysunięty punkt Afryki?", FirstAnswer = "Przylądek Hornless", SecondAnswer = "Przylądek Horn", ThirdQuestion = "Przylądek Igielny", FourthAnswer = "Przylądek Dobrej Nadziei", QuizChallengeIdenty = 227, CorrectAnswer = "Przylądek Igielny", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Na jakim kontynencie leży pustynia Gobi?", FirstAnswer = " w Ameryce Południowej", SecondAnswer = "w Afryce", ThirdQuestion = "w Europie", FourthAnswer = "w Azji", QuizChallengeIdenty = 227, CorrectAnswer = "w Azji", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak nazywa się najdłuższa rzeka w Europie?", FirstAnswer = "Ren", SecondAnswer = "Wisła", ThirdQuestion = "Wołga", FourthAnswer = "Don", QuizChallengeIdenty = 227, CorrectAnswer = "Wołga", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Nazwa jeziora nad którym znajduje się Mysia Wieża, w której według legendy myszy zjadły Popiela?", FirstAnswer = "Gopło", SecondAnswer = "Jeziorak", ThirdQuestion = "Bajkał", FourthAnswer = "Lake City", QuizChallengeIdenty = 227, CorrectAnswer = "Gopło", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Nad jaką rzeką leży Wiedeń?", FirstAnswer = "nad Donem", SecondAnswer = "nad Renem", ThirdQuestion = "nad Mozaną", FourthAnswer = "nad Dunajem", QuizChallengeIdenty = 228, CorrectAnswer = "nad Dunajem", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Plitwickie Jeziora znajdują się na terytorium", FirstAnswer = "Grecji", SecondAnswer = "Albanii", ThirdQuestion = "Chorwacji", FourthAnswer = "Słowenii", QuizChallengeIdenty = 228, CorrectAnswer = "Chorwacji", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Kto jest autorem piosenki Hey Jude?", FirstAnswer = "Led Zeppelin", SecondAnswer = "The Beatles ", ThirdQuestion = "AC/DC", FourthAnswer = "Republika", QuizChallengeIdenty = 228, CorrectAnswer = "The Beatles ", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak nazywa się telefon produkowany przez amerykański koncern Apple?", FirstAnswer = "Iphone", SecondAnswer = "Ipad", ThirdQuestion = "Ijox", FourthAnswer = "Phone", QuizChallengeIdenty = 228, CorrectAnswer = "Iphone", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Częścią którego morza jest Zatoka Botnicka?", FirstAnswer = "Azowskiego", SecondAnswer = "Atlantyku", ThirdQuestion = "Bałtyku", FourthAnswer = "Czarnego", QuizChallengeIdenty = 228, CorrectAnswer = "Bałtyku", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak nazywa się jedyny niezamieszkały przez człowieka kontynent?", FirstAnswer = "Arktyka", SecondAnswer = "Antarktyka", ThirdQuestion = "Atlantyda", FourthAnswer = "Antarktyda", QuizChallengeIdenty = 228, CorrectAnswer = "Antarktyda", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak nazywaja się łąki górskie w Bieszczadch?", FirstAnswer = "Połoniny", SecondAnswer = "Hale", ThirdQuestion = "Pampy", FourthAnswer = "Lubarwy", QuizChallengeIdenty = 228, CorrectAnswer = "Połoniny", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Regiel to piętro roślinności w?", FirstAnswer = "w Bieszczadach", SecondAnswer = "Tatrach", ThirdQuestion = "Alpach", FourthAnswer = "Karpatach", QuizChallengeIdenty = 228, CorrectAnswer = "Tatrach", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jakie miasto jest stolicą Australii ?", FirstAnswer = "Canberra", SecondAnswer = "Perth", ThirdQuestion = "Sydney", FourthAnswer = "Wiedeń", QuizChallengeIdenty = 228, CorrectAnswer = "Canberra", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak nazywa się miasto w którym urodził się Mikołaj Kopernik", FirstAnswer = "Toruń", SecondAnswer = "Kraków", ThirdQuestion = "Frombork", FourthAnswer = "Frampol", QuizChallengeIdenty = 228, CorrectAnswer = "Toruń", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Odmiana czasownika przez osoby, to:", FirstAnswer = "Deklinacja", SecondAnswer = "Koniugacja", ThirdQuestion = "Rzeczwonik", FourthAnswer = "przysłówek", QuizChallengeIdenty = 229, CorrectAnswer = "Koniugacja", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Na pytanie Kto? Co?, odpowiada:", FirstAnswer = "rzeczownik", SecondAnswer = "czasownik", ThirdQuestion = "partykuła", FourthAnswer = "wykrzyknienie", QuizChallengeIdenty = 229, CorrectAnswer = "rzeczownik", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Odmiana czasownika przez przypadki, to:", FirstAnswer = "deklinacja", SecondAnswer = "koniugacja", ThirdQuestion = "przydawka", FourthAnswer = "zdrobnienie", QuizChallengeIdenty = 229, CorrectAnswer = "deklinacja", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "częścią mowy nie jest", FirstAnswer = "przymiotnik", SecondAnswer = "czasownik", ThirdQuestion = "rzeczownik", FourthAnswer = "okolicznik", QuizChallengeIdenty = 229, CorrectAnswer = "okolicznik", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "wyraz bliskoznaczny, to", FirstAnswer = "antonim", SecondAnswer = "zgrubienie", ThirdQuestion = "synonim", FourthAnswer = "antropomorfizm", QuizChallengeIdenty = 229, CorrectAnswer = "synonim", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "które słowo jest partykułą?", FirstAnswer = "ser", SecondAnswer = "zgubiwszy", ThirdQuestion = "zjadłszy", FourthAnswer = "dajże", QuizChallengeIdenty = 229, CorrectAnswer = "dajże", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "czasownik w 1 osobie, to:", FirstAnswer = "zroniłem", SecondAnswer = "zrobiliśmy", ThirdQuestion = "zrobili", FourthAnswer = "zrobić", QuizChallengeIdenty = 229, CorrectAnswer = "zrobiłem", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "na pytanie co robi? Co się z nim dzieje? W jakim jest stanie, odpowiada:", FirstAnswer = "czasownik", SecondAnswer = "przymiotnik", ThirdQuestion = "dopełnienie", FourthAnswer = "przysłówek", QuizChallengeIdenty = 229, CorrectAnswer = "czasownik", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "ile przypadków wyróżniamy w języku polskim?", FirstAnswer = "7", SecondAnswer = "8", ThirdQuestion = "4", FourthAnswer = "9", QuizChallengeIdenty = 229, CorrectAnswer = "7", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "zgrubieniem nie jest:", FirstAnswer = "psisko", SecondAnswer = "marchewa", ThirdQuestion = "koteczek", FourthAnswer = "kocisko", QuizChallengeIdenty = 229, CorrectAnswer = "koteczek", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "głównym boharerem powiesci Chłopi był:", FirstAnswer = "Maciej Boryna", SecondAnswer = "Zofia Naukowska", ThirdQuestion = "Hamlet", FourthAnswer = "Zosia Horeszko", QuizChallengeIdenty = 230, CorrectAnswer = "Maciej Boryna", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Z jakiej epoki wywodzi się utwór pt.  Cierpienia młodego Wertera?", FirstAnswer = "Oświecenie", SecondAnswer = "Romantyzm", ThirdQuestion = "Młoda Polska", FourthAnswer = "Pozytywizm", QuizChallengeIdenty = 230, CorrectAnswer = "Romantyzm", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Autorem Świętoszka jest:", FirstAnswer = "Molier", SecondAnswer = "Wisława Szymborska", ThirdQuestion = "Adam Mickiewicz", FourthAnswer = "Juliusz Słowacki", QuizChallengeIdenty = 230, CorrectAnswer = "Molier", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Autorem wiersza  Z pamiętnika Zofii Bobrówny jest:", FirstAnswer = "Wisława Szymborska", SecondAnswer = "Cyprian Kamil Norwid", ThirdQuestion = "Juliusz Słowacki", FourthAnswer = "Dorota Rabczewska", QuizChallengeIdenty = 230, CorrectAnswer = "Juliusz Słowacki", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Symbolem mędrca w Odzie do Młodości jest:", FirstAnswer = "serce", SecondAnswer = "szkiełko i oko", ThirdQuestion = "okulary", FourthAnswer = "wieczne pióro", QuizChallengeIdenty = 230, CorrectAnswer = "szkiełko i oko", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Miej serce i patrzaj w serce, to cytat z:", FirstAnswer = "Ody do Młodości", SecondAnswer = "Romea i Julii", ThirdQuestion = "Kota w pustym mieszkaniu", FourthAnswer = "Scherzo", QuizChallengeIdenty = 230, CorrectAnswer = "Ody do Młodości", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Tango, to utwór", FirstAnswer = "Mickiewicza", SecondAnswer = "Mrożka", ThirdQuestion = "Witkacego", FourthAnswer = "Dostojewskiego", QuizChallengeIdenty = 230, CorrectAnswer = "Mrożka", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Nadanie cech ludzkich zwierzętom, to;", FirstAnswer = "Inwokacja", SecondAnswer = "Apostrofa", ThirdQuestion = "Anafora", FourthAnswer = "Uosobienie", QuizChallengeIdenty = 230, CorrectAnswer = "Uosobienie", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Powtórzenie na koncu zdań, to:", FirstAnswer = "Epifora", SecondAnswer = "Anafora", ThirdQuestion = "Metafora", FourthAnswer = "Apostrofa", QuizChallengeIdenty = 230, CorrectAnswer = "Epifora", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Wisława Szymborska zmarła w roku:", FirstAnswer = "2009", SecondAnswer = "2013", ThirdQuestion = "2011", FourthAnswer = "2010", QuizChallengeIdenty = 230, CorrectAnswer = "2011", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Kim był Filemon?", FirstAnswer = "Psem", SecondAnswer = "Kretem", ThirdQuestion = "Kotem", FourthAnswer = "Krową", QuizChallengeIdenty = 231, CorrectAnswer = "Kotem", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Brat Bolka, to:", FirstAnswer = "Dobromir", SecondAnswer = "Lolek", ThirdQuestion = "Maciej", FourthAnswer = "Bolek nie miał brata", QuizChallengeIdenty = 231, CorrectAnswer = "Lolek", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Przyjacielem Misia Uszatka nie był", FirstAnswer = "króleczek", SecondAnswer = "prosiaczek", ThirdQuestion = "lalki", FourthAnswer = "myszka", QuizChallengeIdenty = 231, CorrectAnswer = "myszka", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "W bajce Tom and Jerry, Jerry był:", FirstAnswer = "kotem", SecondAnswer = "ptakiem", ThirdQuestion = "myszą", FourthAnswer = "szczurem", QuizChallengeIdenty = 231, CorrectAnswer = "myszą", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Symbolem Zorro była litera:", FirstAnswer = "A", SecondAnswer = "Z", ThirdQuestion = "V", FourthAnswer = "X", QuizChallengeIdenty = 231, CorrectAnswer = "Z", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "W bajce Filemon i przyjaciele, wies odwiedziła ciocia. Jak miała na imię?", FirstAnswer = "Jasia", SecondAnswer = "Jusia", ThirdQuestion = "Cesia", FourthAnswer = "Lusia", QuizChallengeIdenty = 231, CorrectAnswer = "Lusia", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Włosy Czarodziejki z księżyca miały kolor:", FirstAnswer = "różowy", SecondAnswer = "żółty", ThirdQuestion = "czarny", FourthAnswer = "niebieski", QuizChallengeIdenty = 231, CorrectAnswer = "żółty", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Tabaluga był", FirstAnswer = "królikiem", SecondAnswer = "smokiem", ThirdQuestion = "dinozaurem", FourthAnswer = "centaurem", QuizChallengeIdenty = 231, CorrectAnswer = "smokiem", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Gdzie podróżował Koziołek Matołek?", FirstAnswer = "Do Warszawy", SecondAnswer = "Do Sosnowca", ThirdQuestion = "Do Gdynii", FourthAnswer = "Do Pacanowa", QuizChallengeIdenty = 231, CorrectAnswer = "Do Pacanowa", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Było sobie życie - to kreskówka wyprodukowana w jakim kraju?", FirstAnswer = "Norwegia", SecondAnswer = "Francja", ThirdQuestion = "Niemcy", FourthAnswer = "Czechy", QuizChallengeIdenty = 231, CorrectAnswer = "Francja", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak miała na imię mama muminka?", FirstAnswer = "Fizia", SecondAnswer = "Lota", ThirdQuestion = "Mama Muminka", FourthAnswer = "Kisielowa", QuizChallengeIdenty = 231, CorrectAnswer = "Mama Muminka", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Murzynek Bambo myślał, że się wybieli, gdy:", FirstAnswer = "napije się mleka", SecondAnswer = "zje kawałek białego sera", ThirdQuestion = "weźmie kąpiel", FourthAnswer = "założy białe ubranko", QuizChallengeIdenty = 232, CorrectAnswer = "weźmie kąpiel", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Skąpiec z Opowieści Wigilijnej, nazywał się:", FirstAnswer = "Ebenezer Scrooge", SecondAnswer = "Harry Potter", ThirdQuestion = "Jakub Marley", FourthAnswer = "Johnny Bravo", QuizChallengeIdenty = 232, CorrectAnswer = "Ebenezer Scrooge", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Przyjacielem Harrego Pottera był:", FirstAnswer = "Draco Malfoy", SecondAnswer = "Ron Weasley", ThirdQuestion = "Severus Snape", FourthAnswer = "Prawie Bezgłowy Nick", QuizChallengeIdenty = 232, CorrectAnswer = "Ron Weasley", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Gumisie były sok z:", FirstAnswer = "gumijabłek", SecondAnswer = "gumijagód", ThirdQuestion = "gumijeżyn", FourthAnswer = "gumipomidorów", QuizChallengeIdenty = 232, CorrectAnswer = "gumijagód", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Szrek zakochał się w:", FirstAnswer = "Fionie", SecondAnswer = "Fjiłku", ThirdQuestion = "Filipince", FourthAnswer = "Fiannie", QuizChallengeIdenty = 232, CorrectAnswer = "Fionie", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Ojciec Simby miał na imię:", FirstAnswer = "Skaza", SecondAnswer = "Tryton", ThirdQuestion = "Marcel", FourthAnswer = "Mufasa", QuizChallengeIdenty = 232, CorrectAnswer = "Mufasa", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Syn Rumcajsa i Hanki, to:", FirstAnswer = "Józek", SecondAnswer = "Cypisek", ThirdQuestion = "Cyryl", FourthAnswer = "Marcel", QuizChallengeIdenty = 232, CorrectAnswer = "Cypisek", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Roszpunka miała magiczną moc, zawartą w:", FirstAnswer = "rękach", SecondAnswer = "włosach", ThirdQuestion = "sercu", FourthAnswer = "oczach", QuizChallengeIdenty = 232, CorrectAnswer = "włosach", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Nos Rudolfa był:", FirstAnswer = "żółty", SecondAnswer = "pomarańczowy", ThirdQuestion = "czerwony", FourthAnswer = "różowy", QuizChallengeIdenty = 232, CorrectAnswer = "czerwony", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Koziołek Matołek zmierzał do:", FirstAnswer = "Pacanowa", SecondAnswer = "Pacołowa", ThirdQuestion = "Pacankowa", FourthAnswer = "Paczkowa", QuizChallengeIdenty = 232, CorrectAnswer = "Pacanowa", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Atak ZSRR na Polskę podczas II Wojny Światowej miał miejsce", FirstAnswer = "17.09.1939", SecondAnswer = "07.09.1939", ThirdQuestion = "27.09.1939", FourthAnswer = "01.09.1939", QuizChallengeIdenty = 233, CorrectAnswer = "17.09.1939", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Jak się nazywał plan ataku III Rzeszy na ZSRR?", FirstAnswer = "Barbarossa", SecondAnswer = "Weiss", ThirdQuestion = "Blau", FourthAnswer = "Welt", QuizChallengeIdenty = 233, CorrectAnswer = "Barbarossa", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Który z władców Królestwa Polskiego dokonał rozbicia dzielnicowego?", FirstAnswer = "Biały", SecondAnswer = "Krzywobrody", ThirdQuestion = "Krzywousty", FourthAnswer = "Jagiełło", QuizChallengeIdenty = 233, CorrectAnswer = "Krzywousty", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Z jakiej dynastii wywodził się Filip II, król Hiszpanii?", FirstAnswer = "Ausburgów", SecondAnswer = "Pamburgów", ThirdQuestion = "Habsburgów", FourthAnswer = "Hansburgów", QuizChallengeIdenty = 233, CorrectAnswer = "Habsburgów", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Na jaką wyspę został wygnany Napoleon Bonaparte?", FirstAnswer = "Etna", SecondAnswer = "Elba", ThirdQuestion = "Emta", FourthAnswer = "Sycylia", QuizChallengeIdenty = 233, CorrectAnswer = "Elba", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Z jakiej dynastii wywodził się Zygmunt I Stary?", FirstAnswer = "Jagiellonowie", SecondAnswer = "Wazowie", ThirdQuestion = "Welleci", FourthAnswer = "Piastowie", QuizChallengeIdenty = 233, CorrectAnswer = "Jagiellonowie", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "W którym była bitwa pod Warną? ", FirstAnswer = "1444", SecondAnswer = "1440", ThirdQuestion = "1410", FourthAnswer = "1414", QuizChallengeIdenty = 233, CorrectAnswer = "1444", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Kto wypowiedział słowa: Veni, Vidi, Deus vici? ", FirstAnswer = "Kazimierz Jagielończyk", SecondAnswer = "Władysław Wareńczyk", ThirdQuestion = "Jan III Sobieski", FourthAnswer = "Jan II Kazimierz", QuizChallengeIdenty = 233, CorrectAnswer = "Jan III Sobieski", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Kto był następcą Leonida Breżniewa na stanowisku sekretarza KPRZ", FirstAnswer = "Michaił Gorbaczow", SecondAnswer = "Konstantin Czernienko", ThirdQuestion = "Nikita Chruszczow", FourthAnswer = "Jurij Andropow", QuizChallengeIdenty = 233, CorrectAnswer = "Jurij Andropow", IsComplete = false });
            toAddList.Add(new QuizQuestion { Question = "Ile trwała wojna stuletnia?", FirstAnswer = "98 lat", SecondAnswer = "116 lat", ThirdQuestion = "100 lat", FourthAnswer = "103 lata", QuizChallengeIdenty = 233, CorrectAnswer = "116 lat", IsComplete = false });




            db.QuizQuestions.InsertAllOnSubmit(toAddList);
            db.SubmitChanges();

 

            List<QuizChallenge> toAddQuizChallengeList = new List<QuizChallenge> { };
            toAddQuizChallengeList.Add(new QuizChallenge { Title = "Geografia", QuizChallengeId = 227, IsComplete = false, IsSuccessfullyDone = false });
            toAddQuizChallengeList.Add(new QuizChallenge { Title = "Wiedza ogólna", QuizChallengeId = 228, IsComplete = false, IsSuccessfullyDone = false });
            toAddQuizChallengeList.Add(new QuizChallenge { Title = "Gramatyka języka polskiego", QuizChallengeId = 229, IsComplete = false, IsSuccessfullyDone = false });
            toAddQuizChallengeList.Add(new QuizChallenge { Title = "Literatura polska i światowa", QuizChallengeId = 230, IsComplete = false, IsSuccessfullyDone = false });
            toAddQuizChallengeList.Add(new QuizChallenge { Title = "Bajki i animacje I", QuizChallengeId = 231, IsComplete = false, IsSuccessfullyDone = false });
            toAddQuizChallengeList.Add(new QuizChallenge { Title = "Bajki i animacje II", QuizChallengeId = 232, IsComplete = false, IsSuccessfullyDone = false });
            toAddQuizChallengeList.Add(new QuizChallenge { Title = "Historia", QuizChallengeId = 233, IsComplete = false, IsSuccessfullyDone = false });

            db.QuizChallenges.InsertAllOnSubmit(toAddQuizChallengeList);
            db.SubmitChanges();

            dbUpdater.DatabaseSchemaVersion = 2;
            dbUpdater.Execute();
        }



    }
}
