﻿using SpaceQuiz.Contracts;
using SpaceQuiz.DataAccessLayer;
using System.IO;
using System.Linq;
using System.IO.IsolatedStorage;
using Newtonsoft.Json;
using SpaceQuiz.Helper.GameAssistant;
using SpaceQuiz.Model;
using System;

namespace SpaceQuiz.Services
{
    public class GameAssistantService : IGameAssistantService
    {
        private readonly IHttpManager httpManager;

        public GameAssistantService(IHttpManager _httpManager)
        {
            httpManager = _httpManager;
        }


        public async void AddEvent(int EventId)
        {
            var requestUrl = string.Format("{0}/Events/", GameAssistantConfig.ServiceAddress);
            AchievedEvent eventToAdd = new AchievedEvent { EventId = EventId, SourceUserId = App.User.SourceUserId, UserId = App.User.UserId, ProjectId = App.User.ProjectId };
            var data = JsonConvert.SerializeObject(eventToAdd);

            AchievedEvent AchievedEventResponse = null;

            try
            {
                var result = await httpManager.PostAsync(requestUrl, data, true, GameAssistantConfig.Username, GameAssistantConfig.Password);
                result.EnsureSuccessStatusCode();
                var responseString = await result.Content.ReadAsStringAsync();
                AchievedEventResponse = JsonConvert.DeserializeObject<AchievedEvent>(responseString);
            }
            catch (Exception exception)
            {

            }
        }

        public async void AddAchievement(int AchievementId)
        {
            var requestUrl = string.Format("{0}/Achievements/", GameAssistantConfig.ServiceAddress);
            AchievedAchievement achievementToAdd = new AchievedAchievement { AchievementId=AchievementId,  SourceUserId = App.User.SourceUserId, UserId = App.User.UserId, ProjectId = App.User.ProjectId };
            var data = JsonConvert.SerializeObject(achievementToAdd);

            AchievedAchievement AchievedAchievement = null;

            try
            {
                var result = await httpManager.PostAsync(requestUrl, data, true, GameAssistantConfig.Username, GameAssistantConfig.Password);
                result.EnsureSuccessStatusCode();
                var responseString = await result.Content.ReadAsStringAsync();
                AchievedAchievement = JsonConvert.DeserializeObject<AchievedAchievement>(responseString);
            }
            catch (Exception exception)
            {

            }
        }
    }
}
