﻿using SpaceQuiz.Contracts;
using System;
using System.Diagnostics;
using System.Net;

using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Web.Http;

namespace SpaceQuiz.Services
{
    public class HttpManager : IHttpManager
    {
        public async Task<HttpResponseMessage> GetAsync(string url, bool withBasicAuthentication = false, string username = null, string password = null, CancellationTokenSource cancellationTokenSource = null)
        {
            var httpClient = new Windows.Web.Http.HttpClient();

            if (withBasicAuthentication)
                httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + GetBasicCredentials(username, password));



            HttpResponseMessage response = null;
            try
            {
                if (cancellationTokenSource != null)
                    response = await httpClient.GetAsync(new Uri(url + "?nocahce=" + DateTime.Now, UriKind.RelativeOrAbsolute));
                else
                    response = await httpClient.GetAsync(new Uri(url + "?nocahce=" + DateTime.Now, UriKind.RelativeOrAbsolute));
            }
            catch (TaskCanceledException)
            {
                if (Debugger.IsAttached)
                    Debug.WriteLine("Http request canceled.");
            }
            return response;
        }


        public async Task<HttpResponseMessage> PostAsync(string url, string data, bool withBasicAuthentication = false, string username = null, string password = null, CancellationTokenSource cancellationTokenSource = null)
        {
            var httpClient = new HttpClient();
            if (withBasicAuthentication)
            {
                httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + GetBasicCredentials(username, password));
            }

            HttpResponseMessage response = null;
            try
            {
                if (cancellationTokenSource != null)
                    response = await httpClient.PostAsync(new Uri(url + "?nocahce=" + DateTime.Now), new HttpStringContent(data, Windows.Storage.Streams.UnicodeEncoding.Utf8, "application/json"));
                else
                    response = await httpClient.PostAsync(new Uri(url + "?nocahce=" + DateTime.Now), new HttpStringContent(data, Windows.Storage.Streams.UnicodeEncoding.Utf8, "application/json"));
            }
            catch (TaskCanceledException)
            {
                if (Debugger.IsAttached)
                    Debug.WriteLine("Http request canceled.");
            }
            return response;
        }

        private string GetBasicCredentials(string username, string password)
        {
            string mergedCredentials = string.Format("{0}:{1}", username, password);
            byte[] byteCredentials = Encoding.UTF8.GetBytes(mergedCredentials);
            return Convert.ToBase64String(byteCredentials);
        }

    }
}
