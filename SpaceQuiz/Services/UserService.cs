﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using SpaceQuiz.Model;
using SpaceQuiz.Contracts;
using System.Collections.ObjectModel;
using SpaceQuiz.Helper.GameAssistant;
using Windows.Web.Http;
using System.Xml;


namespace SpaceQuiz.Services
{
    public class UserService : IUserService
    {
        private readonly IHttpManager httpManager;
        private readonly IDialogService dialogService;
        //private readonly ICacheService cacheService;
        public UserService(IHttpManager _httpManager, IDialogService _dialogService)//, CacheService _cacheService)
        {
            //cacheService = _cacheService;
            httpManager = _httpManager;
            dialogService = _dialogService;
        }


        public async Task<ObservableCollection<User>> GetUsers(bool withoutCache = false)
        {
            const string cacheFileName = "Users.cache";

            //List<User> o = new List<User>();

            var requestUrl = string.Format("{0}/Users/{1}", GameAssistantConfig.ServiceAddress, GameAssistantConfig.ProjectId);
            var users = new ObservableCollection<User>();
            try
            {
                var response = await httpManager.GetAsync(requestUrl, true, GameAssistantConfig.Username, GameAssistantConfig.Password);
                response.EnsureSuccessStatusCode();
                var responseStr = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrEmpty(responseStr)) return new ObservableCollection<User>();

                users = JsonConvert.DeserializeObject<ObservableCollection<User>>(responseStr);

                //cacheService.Save(cacheFileName, responseStr);
            }
            catch (Exception exception)
            {

            }

            for (int i = 0; i < users.Count; i++)
            {
                users[i].PlaceInTable = (i + 1).ToString();
                if (App.User != null && users[i].UserId == App.User.UserId)
                    users[i].PlayerColor = "green";
            }

            return users;

            //using (HttpClient client = new HttpClient())
            //{
            //    client.DefaultRequestHeaders.Add("Authorization", "basic " + GameAssistantConfig.AuthToken);
            //    using (HttpResponseMessage response = await client.GetAsync(new Uri(GameAssistantConfig.ServiceAddress+"/Users/")))
            //    {
                    
            //        if (response.IsSuccessStatusCode)
            //        {
            //            string content = await response.Content.ReadAsStringAsync();
            //            o = JsonConvert.DeserializeObject<List<User>>(content);
            //        }
            //    }
            //}
            //return o;
        }

        public async Task<User> GetUserBySourceUserId(string SourceUserId, bool withoutCache = false)
        {
            
            var cacheFileName = string.Format("UserBySourceUserId.{0}.cache", SourceUserId);

            //Check cache file is exist            
            //var isCacheExist = cacheService.DoesFileExist(cacheFileName);
            //if (!withoutCache && isCacheExist)
            //{
            //    var userFile = cacheService.Read(cacheFileName);
            //    return JsonConvert.DeserializeObject<User>(userFile);
            //}

            var requestUrl = string.Format("{0}/Users/{1}/{2}", GameAssistantConfig.ServiceAddress, GameAssistantConfig.ProjectId, SourceUserId);
            User user = null;
            try
            {
                HttpResponseMessage response = await httpManager.GetAsync(requestUrl, true, GameAssistantConfig.Username, GameAssistantConfig.Password);
                response.EnsureSuccessStatusCode();
                var responseStr = await response.Content.ReadAsStringAsync();
                user = JsonConvert.DeserializeObject<User>(responseStr);

                //Save new data to the cache
                //cacheService.Save(cacheFileName, responseStr);
            }
            catch (Exception exception)
            {

            }
            return user;
        }

        //public async Task<User> GetUserBySourceUserId(string SourceUserId)
        //{
        //    SourceUserId = TextTools.Base64Encode(SourceUserId);

        //    User o = new User();

        //    using (HttpClient client = new HttpClient())
        //    {
        //        client.DefaultRequestHeaders.Add("Authorization", "basic " + GameAssistantConfig.AuthToken);
        //        using (HttpResponseMessage response = await client.GetAsync(new Uri(GameAssistantConfig.ServiceAddress + "/Users/" + GameAssistantConfig.ProjectId + "/3")))
        //        {

        //            if (response.IsSuccessStatusCode)
        //            {
        //                string content = await response.Content.ReadAsStringAsync();
        //                o = JsonConvert.DeserializeObject<User>(content);
        //            }
        //        }
        //    }
        //    return o;
        //}



        public async Task<User> CreateUser(User user)
        {
            var requestUrl = string.Format("{0}/Users/", GameAssistantConfig.ServiceAddress);
            var data = JsonConvert.SerializeObject(user);
            
            User createUserResponse = null;

            try
            {
                var result = await httpManager.PostAsync(requestUrl, data, true, GameAssistantConfig.Username, GameAssistantConfig.Password);
                result.EnsureSuccessStatusCode();
                var responseString = await result.Content.ReadAsStringAsync();
                createUserResponse = JsonConvert.DeserializeObject<User>(responseString);
            }
            catch (Exception exception)
            {

            }
            return createUserResponse;
        }


        public async void ReloadAppUser()
        {
            var user = await GetUserBySourceUserId(App.SourceUserId);
            if (user.LevelId != App.User.LevelId)
                dialogService.ShowDialog("Zdobyłeś nowy poziom! Od teraz masz rangę " + user.CurrentLevel.LevelTitle, "Gratulacje");
            foreach (var item in user.AchievedAchievements)
            {
                if (App.User.AchievedAchievements.FirstOrDefault(x => x.AchievedAchievementId == item.AchievedAchievementId) == null)
                    dialogService.ShowDialog("Zdobyłeś nowe osiągnięcie - " + item.Achievement.Title + " (+"+item.PointsAchieved+"pkt)", "Gratulacje");
            }
            App.User = user;
        }


        public async Task<ObservableCollection<User>> GetShortPlayersList()
        {
            var users = await GetUsers();

            bool playerIsInTop3 = false;
            ObservableCollection<User> newUsers = new ObservableCollection<User>();
            foreach (var item in users.Take(3))
            {
                if (item.UserName == App.User.UserName)
                    playerIsInTop3 = true;
                newUsers.Add(item);
            }

            if (!playerIsInTop3)
            {
                if (users.FirstOrDefault(x => x.UserName == App.User.UserName).PlaceInTable != "4")
                {
                    newUsers.Add(new User { PlaceInTable = "...", UserName = "..." });
                    var userInTable = users.FirstOrDefault(x => x.UserName == App.User.UserName);
                    int placeInTable = Int32.Parse(userInTable.PlaceInTable);

                    if (users.FirstOrDefault(x => x.PlaceInTable == (placeInTable - 1).ToString()) != null)
                        newUsers.Add(users.FirstOrDefault(x => x.PlaceInTable == (placeInTable - 1).ToString()));

                    newUsers.Add(users.FirstOrDefault(x => x.UserName == App.User.UserName));

                    if (users.FirstOrDefault(x => x.PlaceInTable == (placeInTable + 1).ToString()) != null)
                        newUsers.Add(users.FirstOrDefault(x => x.PlaceInTable == (placeInTable + 1).ToString()));
                }
                else
                    newUsers.Add(users.FirstOrDefault(x => x.UserName == App.User.UserName));
            }
            else
            {
                newUsers.Add(users.ElementAt(3));
            }




            return newUsers;

        }
    }
}
