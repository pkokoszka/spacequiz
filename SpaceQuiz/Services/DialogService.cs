﻿using Microsoft.Phone.Controls;
using SpaceQuiz.Contracts;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SpaceQuiz.Services
{
    public class DialogService : IDialogService
    {
        public void ShowDialog(string message, string caption)
        {
            MessageBox.Show(message, caption, MessageBoxButton.OK);
        }     
    }
}
