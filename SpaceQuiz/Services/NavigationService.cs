﻿using Microsoft.Phone.Controls;
using SpaceQuiz;
using SpaceQuiz.Common;
using SpaceQuiz.Contracts;
using SpaceQuiz.ViewModel;
using System;
using System.Collections.Generic;
using System.Windows;

namespace SpaceQuiz.Services
{
    /// <summary> 
    /// The navigation service. 
    /// </summary> 
    public class NavigationService : INavigationService
    {
        private static readonly Dictionary<Type, string> ViewModelRouting = new Dictionary<Type, string>
        {
                                                                          { 
                                                                              typeof(MarathonQuestionViewModel), ViewNames.MarathonQuestionView
                                                                              }, 
                                                                          { 
                                                                              typeof(MarathonIntroViewModel), ViewNames.MarathonIntroView
                                                                              }, 
                                                                          { 
                                                                              typeof(WelcomeViewModel), ViewNames.WelcomeView
                                                                              }, 
                                                                          { 
                                                                              typeof(PlayerProfileViewModel), ViewNames.PlayerProfileView
                                                                              }, 
                                                                          { 
                                                                              typeof(RegisterUserViewModel), ViewNames.RegisterUserView
                                                                              },
                                                                              { 
                                                                              typeof(ContactViewModel), ViewNames.ContactView
                                                                              },
                                                                              { 
                                                                              typeof(ChallengeListViewModel), ViewNames.ChallengeListView
                                                                              },
                                                                              { 
                                                                              typeof(ChallengeIntroViewModel), ViewNames.ChallengeIntroView
                                                                              },
                                                                              { 
                                                                              typeof(ChallengeQuestionViewModel), ViewNames.ChallengeQuestionView
                                                                              },
                                                                              { 
                                                                              typeof(PlayersTableViewModel), ViewNames.PlayersTableView
                                                                              }
        };

        public bool CanGoBack
        {
            get
            {
                return App.RootFrame.CanGoBack;
            }
        }


        public object NavigationParameter
        {
            get
            {
                return App.NavigationParameter;
            }
            set
            {
                App.NavigationParameter = value;
            }
        }

        public void GoBack()
        {
            App.RootFrame.GoBack();
        }

        public void Navigate<TDestinationViewModel>(string parameter)
        {
            var navParameter = string.Empty;
            if (parameter != null)
            {
                navParameter = "?param=" + parameter;
            }

            if (ViewModelRouting.ContainsKey(typeof(TDestinationViewModel)))
            {
                var page = ViewModelRouting[typeof(TDestinationViewModel)];

                App.RootFrame.Navigate(new Uri("/" + page + navParameter, UriKind.Relative));
            }
        }

        public void Navigate<TDestinationViewModel>(object parameter)
        {
            if (parameter != null)
            {
                NavigationParameter = parameter;
            }

            if (ViewModelRouting.ContainsKey(typeof(TDestinationViewModel)))
            {
                var page = ViewModelRouting[typeof(TDestinationViewModel)];

                App.RootFrame.Navigate(new Uri("/" + page, UriKind.Relative));
            }
        }

        public void RemoveBackEntry()
        {
            App.RootFrame.RemoveBackEntry();
        }

        public object GetNavigationParameter()
        {
            return NavigationParameter;
        }
    }
}
