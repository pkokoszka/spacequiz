﻿using SpaceQuiz.Contracts;
using SpaceQuiz.DataAccessLayer;
using System.IO;
using System.Linq;
using System.IO.IsolatedStorage;
using System;
using System.Collections.ObjectModel;
using SpaceQuiz.Model;

namespace SpaceQuiz.Services
{
    public class SpaceQuizService : ISpaceQuizService
    {
        private readonly SpaceQuizDataContext db;


        public SpaceQuizService()
        {
            db = new SpaceQuizDataContext(SpaceQuizDataContext.DBConnectionString);
        }


        public Model.QuizQuestion GetRandomQuestion()
        {
            var qry = from row in db.QuizQuestions
                      where row.IsComplete == false
                      select row;

            int count = qry.Count(); // 1st round-trip
            int index = new Random().Next(count);

            return qry.Skip(index).FirstOrDefault(); 
        }


        public void MarkQuestionAsCompleted(int QuizQuestionId)
        {
            var question = db.QuizQuestions.FirstOrDefault(x => x.QuizQuestionId == QuizQuestionId);
            question.IsComplete = true;
            db.SubmitChanges();
        }


        public System.Collections.ObjectModel.ObservableCollection<Model.QuizChallenge> GetDoneChallenges()
        {
            var list = db.QuizChallenges.Where(x => x.IsComplete == true).ToList();
            ObservableCollection<QuizChallenge> toReturn = new ObservableCollection<QuizChallenge>();
            foreach (var item in list)
            {
                toReturn.Add(item);
            }
            return toReturn;
        }

        public System.Collections.ObjectModel.ObservableCollection<Model.QuizChallenge> GetToDoChallenges()
        {
            var list = db.QuizChallenges.Where(x => x.IsComplete == false).ToList();
            ObservableCollection<QuizChallenge> toReturn = new ObservableCollection<QuizChallenge>();
            foreach (var item in list)
            {
                toReturn.Add(item);
            }
            return toReturn;
        }


        public QuizChallenge GetQuizChallengeById(int QuizChallengeKey)
        {
            return db.QuizChallenges.FirstOrDefault(x => x.QuizChallengeId == QuizChallengeKey);
        }

        public System.Collections.Generic.List<QuizQuestion> GetQuizChallengeQuestionList(int QuizChallengeKey)
        {
            return db.QuizQuestions.Where(x => x.QuizChallengeIdenty == QuizChallengeKey).Take(10).ToList();
        }


        public void MarkChallengeAsCompleted(int QuizChallengeKey)
        {
            var entity = GetQuizChallengeById(QuizChallengeKey);
            entity.IsComplete = true;
            entity.IsSuccessfullyDone = true;

            db.SubmitChanges();
        }
    }
}
