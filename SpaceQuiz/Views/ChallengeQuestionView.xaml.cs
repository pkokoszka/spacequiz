﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using SpaceQuiz.ViewModel;


namespace SpaceQuiz.Views
{
    public partial class ChallengeQuestionView : PhoneApplicationPage
    {
        // Constructor
        public ChallengeQuestionView()
        {
            InitializeComponent();

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }



        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            var vm = this.DataContext as ChallengeQuestionViewModel;
            vm.GoBack();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var vm = this.DataContext as ChallengeQuestionViewModel;
            vm.CleanUp();
            if (NavigationContext.QueryString.ContainsKey("param"))
            {
                var QuizChallengeKey = Int32.Parse(NavigationContext.QueryString["param"]);
                vm.CleanUp();
                vm.Initialize(QuizChallengeKey);
            }
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}