﻿using SpaceQuiz.Model;
using System;
namespace SpaceQuiz.Contracts
{
    public interface IUserService
    {
        System.Threading.Tasks.Task<SpaceQuiz.Model.User> GetUserBySourceUserId(string SourceUserId, bool withoutCache = false);
        System.Threading.Tasks.Task<System.Collections.ObjectModel.ObservableCollection<SpaceQuiz.Model.User>> GetUsers(bool withoutCache = false);

        System.Threading.Tasks.Task<User> CreateUser(Model.User user);

        void ReloadAppUser();

        System.Threading.Tasks.Task<System.Collections.ObjectModel.ObservableCollection<User>> GetShortPlayersList();
    }
}
