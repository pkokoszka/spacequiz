﻿
namespace SpaceQuiz.Contracts
{
    /// <summary>
    /// IQuizQuestionService
    /// </summary>
    public interface ISpaceQuizService
    {


        Model.QuizQuestion GetRandomQuestion();

        void MarkQuestionAsCompleted(int QuizQuestionId);

        System.Collections.ObjectModel.ObservableCollection<Model.QuizChallenge> GetDoneChallenges();

        System.Collections.ObjectModel.ObservableCollection<Model.QuizChallenge> GetToDoChallenges();

        Model.QuizChallenge GetQuizChallengeById(int QuizChallengeKey);

        System.Collections.Generic.List<Model.QuizQuestion> GetQuizChallengeQuestionList(int QuizChallengeKey);

        void MarkChallengeAsCompleted(int QuizChallengeKey);
    }
}
