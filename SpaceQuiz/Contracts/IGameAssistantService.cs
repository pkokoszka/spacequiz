﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceQuiz.Contracts
{
    public interface IGameAssistantService
    {
        void AddEvent(int EventId);

        void AddAchievement(int AchievementId);
    }
}
