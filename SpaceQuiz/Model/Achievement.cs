﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceQuiz.Model
{
    public class Achievement
    {
        public int AchievementId { get; set; }
        public int AchievementType { get; set; } //enums
        public string Title { get; set; }
        public int? PointsForAchieve { get; set; }

        public DateTime CreateDate { get; set; }
        public bool del { get; set; }

        public int? EventsIteration { get; set; }

        public int ConnectedEventId { get; set; }
        public virtual Event Event { get; set; }

        public int ProjectId { get; set; }

        public virtual ICollection<AchievedAchievement> AchievedAchievements { get; set; }
    }
}
