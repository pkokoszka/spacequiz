﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SpaceQuiz.Model
{
    [DataContract]
    public class Event
    {
        [DataMember]
        public int EventId { get; set; }

        [DataMember]
        public string EventNameId { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int PointsForAchieve { get; set; }

        [DataMember]
        public bool MultipleEvent { get; set; }

        public DateTime CreateDate { get; set; }

        public bool del { get; set; }

        public int ProjectId { get; set; }

        public virtual ICollection<AchievedEvent> AchievedEvents { get; set; }

    }
}
