﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SpaceQuiz.Model
{
    [DataContract]
    public class User
    {

        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public string SourceUserId { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public int Points { get; set; }
        [DataMember]
        public int LevelId { get; set; }
        [DataMember]
        public virtual Level CurrentLevel { get; set; }
        [DataMember]
        public string AdditionalData { get; set; }
        [DataMember]
        public DateTime? CreateDate { get; set; }
        public bool del { get; set; }
        [DataMember]
        public int ProjectId { get; set; }

        [DataMember]
        public virtual ICollection<AchievedEvent> AchievedEvents { get; set; }
        [DataMember]
        public virtual ICollection<AchievedAchievement> AchievedAchievements { get; set; }

        public string PointsDesc
        {
            get
            {
                if (PlaceInTable == "...")
                    return "...";
                else
                    return Points.ToString();
            }
        }

        public string PlaceInTable { get; set; }

        public string PlayerColor { get; set; }
    }
}
