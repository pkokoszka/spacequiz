﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SpaceQuiz.Model
{
    [DataContract]
    public class AchievedEvent
    {
        [DataMember]
        public int AchievedEventId { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public string SourceUserId { get; set; }
        public virtual User User { get; set; }
        [DataMember]
        public int EventId { get; set; }
        [DataMember]
        public virtual Event Event { get; set; }

        [DataMember]
        public int PointsAchieved { get; set; }


        [DataMember]
        public int ProjectId { get; set; }
        [DataMember]
        public DateTime? AchieveDate { get; set; }
        [DataMember]
        public bool del { get; set; }
    }
}
