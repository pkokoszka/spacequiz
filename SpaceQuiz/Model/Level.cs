﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SpaceQuiz.Model
{
    [DataContract]
    public class Level
    {
        [DataMember]
        public int LevelId { get; set; }
        [DataMember]
        public string LevelTitle { get; set; }
        [DataMember]
        public int LowerThreshold { get; set; }
        [DataMember]
        public int UpperThreshold { get; set; }
        [DataMember]
        public bool UpperThresholdInf { get; set; }
        public DateTime CreateDate { get; set; }
        public bool del { get; set; }
        public int ProjectId { get; set; }
    }
}
