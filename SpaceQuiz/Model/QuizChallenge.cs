﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceQuiz.Model
{
    [Table]
    public class QuizChallenge
    {
        private int _QuizChallengeId;

        [Column(IsPrimaryKey = true)]
        public int QuizChallengeId
        {
            get
            {
                return _QuizChallengeId;
            }
            set
            {
                if (_QuizChallengeId != value)
                {
                    NotifyPropertyChanging("QuizChallengeId");
                    _QuizChallengeId = value;
                    NotifyPropertyChanged("QuizChallengeId");
                }
            }
        }



        private string _Title;

        [Column]
        public string Title
        {
            get
            {
                return _Title;
            }
            set
            {
                if (_Title != value)
                {
                    NotifyPropertyChanging("Title");
                    _Title = value;
                    NotifyPropertyChanged("Title");
                }
            }
        }

        private bool _isComplete;

        [Column]
        public bool IsComplete
        {
            get
            {
                return _isComplete;
            }
            set
            {
                if (_isComplete != value)
                {
                    NotifyPropertyChanging("IsComplete");
                    _isComplete = value;
                    NotifyPropertyChanged("IsComplete");
                }
            }
        }

        private bool _IsSuccessfullyDone;

        [Column]
        public bool IsSuccessfullyDone
        {
            get
            {
                return _IsSuccessfullyDone;
            }
            set
            {
                if (_IsSuccessfullyDone != value)
                {
                    NotifyPropertyChanging("IsSuccessfullyDone");
                    _IsSuccessfullyDone = value;
                    NotifyPropertyChanged("IsSuccessfullyDone");
                }
            }
        }



        [Column(IsVersion = true)]
        private Binary _version;


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }
}
