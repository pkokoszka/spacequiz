﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceQuiz.Model
{
    [Table]
    public class QuizQuestion : INotifyPropertyChanged, INotifyPropertyChanging
    {
        private int _QuizQuestionId;

        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int QuizQuestionId
        {
            get
            {
                return _QuizQuestionId;
            }
            set
            {
                if (_QuizQuestionId != value)
                {
                    NotifyPropertyChanging("QuizQuestionId");
                    _QuizQuestionId = value;
                    NotifyPropertyChanged("QuizQuestionId");
                }
            }
        }

        private string _Question;

        [Column]
        public string Question
        {
            get
            {
                return _Question;
            }
            set
            {
                if (_Question != value)
                {
                    NotifyPropertyChanging("Question");
                    _Question = value;
                    NotifyPropertyChanged("Question");
                }
            }
        }

        private string _CorrectAnswer;

        [Column]
        public string CorrectAnswer
        {
            get
            {
                return _CorrectAnswer;
            }
            set
            {
                if (_CorrectAnswer != value)
                {
                    NotifyPropertyChanging("CorrectAnswer");
                    _CorrectAnswer = value;
                    NotifyPropertyChanged("CorrectAnswer");
                }
            }
        }


        private string _FourthAnswer;

        [Column]
        public string FourthAnswer
        {
            get
            {
                return _FourthAnswer;
            }
            set
            {
                if (_FourthAnswer != value)
                {
                    NotifyPropertyChanging("FourthAnswer");
                    _FourthAnswer = value;
                    NotifyPropertyChanged("FourthAnswer");
                }
            }
        }


        private string _ThirdQuestion;

        [Column]
        public string ThirdQuestion
        {
            get
            {
                return _ThirdQuestion;
            }
            set
            {
                if (_ThirdQuestion != value)
                {
                    NotifyPropertyChanging("ThirdQuestion");
                    _ThirdQuestion = value;
                    NotifyPropertyChanged("ThirdQuestion");
                }
            }
        }


        private string _SecondAnswer;

        [Column]
        public string SecondAnswer
        {
            get
            {
                return _SecondAnswer;
            }
            set
            {
                if (_SecondAnswer != value)
                {
                    NotifyPropertyChanging("SecondAnswer");
                    _SecondAnswer = value;
                    NotifyPropertyChanged("SecondAnswer");
                }
            }
        }


        private string _FirstAnswer;

        [Column]
        public string FirstAnswer
        {
            get
            {
                return _FirstAnswer;
            }
            set
            {
                if (_FirstAnswer != value)
                {
                    NotifyPropertyChanging("FirstAnswer");
                    _FirstAnswer = value;
                    NotifyPropertyChanged("FirstAnswer");
                }
            }
        }


        private bool _isComplete;

        [Column]
        public bool IsComplete
        {
            get
            {
                return _isComplete;
            }
            set
            {
                if (_isComplete != value)
                {
                    NotifyPropertyChanging("IsComplete");
                    _isComplete = value;
                    NotifyPropertyChanged("IsComplete");
                }
            }
        }


        private int? _QuizChallengeIdenty;

        [Column]
        public int? QuizChallengeIdenty
        {
            get
            {
                return _QuizChallengeIdenty;
            }
            set
            {
                if (_QuizChallengeIdenty != value)
                {
                    NotifyPropertyChanging("QuizChallengeIdenty");
                    _QuizChallengeIdenty = value;
                    NotifyPropertyChanged("QuizChallengeIdenty");
                }
            }
        }



        [Column(IsVersion = true)]
        private Binary _version;

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }



}
