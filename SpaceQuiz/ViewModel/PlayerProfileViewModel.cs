﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using SpaceQuiz.Contracts;
using SpaceQuiz.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace SpaceQuiz.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class PlayerProfileViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IDialogService _dialogService;
        private readonly IUserService _userService;

        private ObservableCollection<User> userList;
        private bool isDataLoaded;
        private string loadingText;
        private User player;

        private bool isTaskbarVisible = true;

        private int placeInRanking;
        private string pointsToNextLevel;


        private string title;
        public string Title
        {
            get { return title; }
            set
            {
                if (title != value)
                {
                    title = value;
                    RaisePropertyChanged(() => Title);
                }
            }
        }


        public int PlaceInRanking
        {
            get { return placeInRanking; }
            set
            {
                if (placeInRanking != value)
                {
                    placeInRanking = value;
                    RaisePropertyChanged(() => PlaceInRanking);
                }
            }
        }
        public string PointsToNextLevel
        {
            get { return pointsToNextLevel; }
            set
            {
                if (pointsToNextLevel != value)
                {
                    pointsToNextLevel = value;
                    RaisePropertyChanged(() => PointsToNextLevel);
                }
            }
        }

        public ObservableCollection<User> UserList
        {
            get { return userList; }
            set
            {
                if (userList != value)
                {
                    userList = value;
                    RaisePropertyChanged(() => UserList);
                }
            }
        }

        public User Player
        {
            get { return player; }
            set
            {
                if (player != value)
                {
                    player = value;
                    RaisePropertyChanged(() => Player);
                }
            }
        }

        public bool IsDataLoaded
        {
            get { return isDataLoaded; }
            set
            {
                if (isDataLoaded != value)
                {
                    isDataLoaded = value;
                    RaisePropertyChanged(() => IsDataLoaded);
                }
            }
        }



        public string LoadingText
        {
            get { return loadingText; }
            set
            {
                if (loadingText != value)
                {
                    loadingText = value;
                    RaisePropertyChanged(() => LoadingText);
                }
            }
        }

        public bool IsTaskbarVisible
        {
            get { return isTaskbarVisible; }
            set
            {
                if (isTaskbarVisible != value)
                {
                    isTaskbarVisible = value;
                    RaisePropertyChanged(() => IsTaskbarVisible);
                }
            }
        }

        public PlayerProfileViewModel(IDialogService dialogService, INavigationService navigationService, IUserService userService)
        {
            
            _dialogService = dialogService;
            _navigationService = navigationService;
            _userService = SimpleIoc.Default.GetInstance<IUserService>(Guid.NewGuid().ToString());//userService;
        }



        public async void Initialize()
        {
            Initialize(App.SourceUserId);
        }

        public void CleanUp()
        {

        }

        private void NavigateToBack()
        {
            CleanUp();
            _navigationService.GoBack();
        }




        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the app that a property has changed.
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        internal async void Initialize(string sourceUserId)
        {
            IsDataLoaded = false;
            Player = await _userService.GetUserBySourceUserId(sourceUserId);
            UserList = await _userService.GetUsers();

            if (player.UserId == 0)
            {
                IsTaskbarVisible = false;
                _dialogService.ShowDialog("Nie jesteś zarejestrowany", "Błąd");
                IsTaskbarVisible = true;
                IsDataLoaded = true;
                return;
            }


            for (int i = 0; i < UserList.Count; i++)
            {
                if (UserList.ElementAt(i).UserId == Player.UserId)
                {
                    PlaceInRanking = i + 1;
                    break;
                }
            }

            int toConvert = Player.CurrentLevel.UpperThreshold + 1 - Player.Points;
            PointsToNextLevel = (toConvert).ToString();
            if (Player.CurrentLevel.UpperThresholdInf)
                PointsToNextLevel = "Osiągnąłeś najwyższy level";

            Title = "Twój profil";
            LoadingText = "Ładuję";
            IsDataLoaded = true;
        }
    }
}