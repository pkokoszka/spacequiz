﻿/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocatorTemplate xmlns:vm="clr-namespace:SpaceQuiz.ViewModel"
                                   x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using SpaceQuiz.Contracts;
using SpaceQuiz.DataAccessLayer;
using SpaceQuiz.Model;
using SpaceQuiz.Services;
using SpaceQuiz.Views;
using System;

namespace SpaceQuiz.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {


            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            SimpleIoc.Default.Register<IHttpManager, HttpManager>();
            SimpleIoc.Default.Register<IUserService, UserService>();
            SimpleIoc.Default.Register<INavigationService, NavigationService>();
            SimpleIoc.Default.Register<IDialogService, DialogService>();
            //SimpleIoc.Default.Register<SpaceQuizDataContext>();
            SimpleIoc.Default.Register<ISpaceQuizService, SpaceQuizService>();
            //SimpleIoc.Default.Register<ICacheService, CacheService>();   
            SimpleIoc.Default.Register<IGameAssistantService, GameAssistantService>();


            SimpleIoc.Default.Register<RegisterUserViewModel>();
            SimpleIoc.Default.Register<WelcomeViewModel>();
            SimpleIoc.Default.Register<PlayerProfileViewModel>();
            SimpleIoc.Default.Register<MarathonIntroViewModel>();
            SimpleIoc.Default.Register<MarathonQuestionViewModel>();
            SimpleIoc.Default.Register<PlayersTableViewModel>();
            SimpleIoc.Default.Register<ContactViewModel>();
            SimpleIoc.Default.Register<AddQuestionViewModel>();
            SimpleIoc.Default.Register<ChallengeIntroViewModel>();
            SimpleIoc.Default.Register<ChallengeListViewModel>();
            SimpleIoc.Default.Register<ChallengeQuestionViewModel>();

            //var navigationService = new NavigationService();
            //navigationService.Configure("MainPage", new Uri("/Views/MainPage.xaml", UriKind.Relative));
            //navigationService.Configure("RegisterUser", new Uri("/Views/RegisterUser.xaml", UriKind.Relative));
            //navigationService.Configure("MvvmView1", new Uri("/Views/MvvmView1.xaml", UriKind.Relative));

            //SimpleIoc.Default.Register<INavigationService>(() => navigationService);

        }
        public ChallengeIntroViewModel ChallengeIntroViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ChallengeIntroViewModel>();
            }
        }


        public ChallengeListViewModel ChallengeListViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ChallengeListViewModel>();
            }
        }

        public RegisterUserViewModel RegisterUserViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<RegisterUserViewModel>();
            }
        }

        public ChallengeQuestionViewModel ChallengeQuestionViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ChallengeQuestionViewModel>();
            }
        }

        public WelcomeViewModel WelcomeViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<WelcomeViewModel>();
            }
        }
        public AddQuestionViewModel AddQuestionViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AddQuestionViewModel>();
            }
        }

        public ContactViewModel ContactViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ContactViewModel>();
            }
        }

        public PlayerProfileViewModel PlayerProfileViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<PlayerProfileViewModel>();
            }
        }

        public MarathonQuestionViewModel MarathonQuestionViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MarathonQuestionViewModel>();
            }
        }

        public PlayersTableViewModel PlayersTableViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<PlayersTableViewModel>();
            }
        }

        public MarathonIntroViewModel MarathonIntroViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MarathonIntroViewModel>();
            }
        }

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public static void Cleanup()
        {

        }
    }
}