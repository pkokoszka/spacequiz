﻿using GalaSoft.MvvmLight;
using SpaceQuiz.Contracts;
using SpaceQuiz.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace SpaceQuiz.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class PlayersTableViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the PlayersTableViewModel class.
        /// </summary>
        /// 
        private readonly IUserService _userService;

        private ObservableCollection<User> _usersList;
        public ObservableCollection<User> UsersList
        {
            get { return _usersList; }
            set
            {
                if (_usersList != value)
                {
                    _usersList = value;
                    RaisePropertyChanged(() => UsersList);
                }
            }
        }


        private ObservableCollection<User> _ShortPlayerList;
        public ObservableCollection<User> ShortPlayerList
        {
            get { return _ShortPlayerList; }
            set
            {
                if (_ShortPlayerList != value)
                {
                    _ShortPlayerList = value;
                    RaisePropertyChanged(() => ShortPlayerList);
                }
            }
        }


        private bool isDataLoaded;
        private bool isTaskbarVisible = true;

        public bool IsDataLoaded
        {
            get { return isDataLoaded; }
            set
            {
                if (isDataLoaded != value)
                {
                    isDataLoaded = value;
                    RaisePropertyChanged(() => IsDataLoaded);
                }
            }
        }

        public bool IsTaskbarVisible
        {
            get { return isTaskbarVisible; }
            set
            {
                if (isTaskbarVisible != value)
                {
                    isTaskbarVisible = value;
                    RaisePropertyChanged(() => IsTaskbarVisible);
                }
            }
        }


        public PlayersTableViewModel(IUserService userService)
        {
            _userService = userService;
        }

        internal async void Initialize()
        {

            IsDataLoaded = true;



            UsersList = await _userService.GetUsers();
            ShortPlayerList = await _userService.GetShortPlayersList();



            ViewModelLocator.Cleanup();
            IsDataLoaded = false;


            IsTaskbarVisible = true;

            IsTaskbarVisible = false;
        }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the app that a property has changed.
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}