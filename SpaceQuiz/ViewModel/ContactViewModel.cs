﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Phone.Tasks;

namespace SpaceQuiz.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ContactViewModel : ViewModelBase
    {
        public RelayCommand SendEmailCommand { get; private set; }

        private string content;
        public string Content
        {
            get { return content; }
            set
            {
                if (content != value)
                {
                    content = value;
                    RaisePropertyChanged(() => Content);
                }
            }
        }


        public ContactViewModel()
        {
            SendEmailCommand = new RelayCommand(SendEmail);
        }

        internal void Initialize()
        {

        }

        public void SendEmail() 
        {
            EmailComposeTask emailComposeTask = new EmailComposeTask();

            emailComposeTask.Subject = "SpaceQuiz: Wiadomość";
            emailComposeTask.Body = Content;
            emailComposeTask.To = "admin@kozacy.org";

            emailComposeTask.Show();
        }
    }
}