﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using SpaceQuiz.Contracts;
using SpaceQuiz.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace SpaceQuiz.ViewModel
{
    public class MarathonQuestionViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IDialogService _dialogService;
        private readonly IUserService _userService;
        private readonly ISpaceQuizService _quizQuestionService;
        private readonly IGameAssistantService _gameAssistantService;

        public RelayCommand ClickFirstAnswerCommand { get; private set; }
        public RelayCommand ClickSecondAnswerCommand { get; private set; }
        public RelayCommand ClickThirdAnswerCommand { get; private set; }
        public RelayCommand ClickFourthAnswerCommand { get; private set; }
        public RelayCommand GoNextCommand { get; set; }




        private int _correctAnswerCountInRow;
        public int CorrectAnswerCountInRow
        {
            get { return _correctAnswerCountInRow; }
            set
            {
                if (_correctAnswerCountInRow != value)
                {
                    _correctAnswerCountInRow = value;
                    RaisePropertyChanged(() => CorrectAnswerCountInRow);
                }
            }
        }

        private int _correctAnswerCount;
        public int CorrectAnswerCount
        {
            get { return _correctAnswerCount; }
            set
            {
                if (_correctAnswerCount != value)
                {
                    _correctAnswerCount = value;
                    RaisePropertyChanged(() => CorrectAnswerCount);
                }
            }
        }

        private int _ticksToEnd;
        public int TicksToEnd
        {
            get { return _ticksToEnd; }
            set
            {
                if (_ticksToEnd != value)
                {
                    _ticksToEnd = value;
                    RaisePropertyChanged(() => TicksToEnd);
                }
            }
        }

        private System.Windows.Visibility _visibility;
        public System.Windows.Visibility Visibility
        {
            get { return _visibility; }
            set
            {
                if (_visibility != value)
                {
                    _visibility = value;
                    RaisePropertyChanged(() => Visibility);
                }
            }
        }

        private bool _isTaskbarVisible = true;
        public bool IsTaskbarVisible
        {
            get { return _isTaskbarVisible; }
            set
            {
                if (_isTaskbarVisible != value)
                {
                    _isTaskbarVisible = value;
                    RaisePropertyChanged(() => IsTaskbarVisible);
                }
            }
        }


        private bool _isDataLoaded;
        public bool IsDataLoaded
        {
            get { return _isDataLoaded; }
            set
            {
                if (_isDataLoaded != value)
                {
                    _isDataLoaded = value;
                    RaisePropertyChanged(() => IsDataLoaded);
                }
            }
        }

        private string _firstAnswerColor;
        public string FirstAnswerColor
        {
            get { return _firstAnswerColor; }
            set
            {
                if (_firstAnswerColor != value)
                {
                    _firstAnswerColor = value;
                    RaisePropertyChanged(() => FirstAnswerColor);
                }
            }
        }

        private string _secondAnswerColor;
        public string SecondAnswerColor
        {
            get { return _secondAnswerColor; }
            set
            {
                if (_secondAnswerColor != value)
                {
                    _secondAnswerColor = value;
                    RaisePropertyChanged(() => SecondAnswerColor);
                }
            }
        }

        private string _fourthAnswerColor;
        public string FourthAnswerColor
        {
            get { return _fourthAnswerColor; }
            set
            {
                if (_fourthAnswerColor != value)
                {
                    _fourthAnswerColor = value;
                    RaisePropertyChanged(() => FourthAnswerColor);
                }
            }
        }

        private string _thirdAnswerColor;
        public string ThirdAnswerColor
        {
            get { return _thirdAnswerColor; }
            set
            {
                if (_thirdAnswerColor != value)
                {
                    _thirdAnswerColor = value;
                    RaisePropertyChanged(() => ThirdAnswerColor);
                }
            }
        }

        private string _messageToPlayer;
        public string MessageToPlayer
        {
            get { return _messageToPlayer; }
            set
            {
                if (_messageToPlayer != value)
                {
                    _messageToPlayer = value;
                    RaisePropertyChanged(() => MessageToPlayer);
                }
            }
        }

        private QuizQuestion _quizQuestion;
        public QuizQuestion QuizQuestion
        {
            get { return _quizQuestion; }
            set
            {
                if (_quizQuestion != value)
                {
                    _quizQuestion = value;
                    RaisePropertyChanged(() => QuizQuestion);
                }
            }
        }


        public MarathonQuestionViewModel(IDialogService dialogService, INavigationService navigationService, IUserService userService, ISpaceQuizService quizQuestionService, IGameAssistantService gameAssistantService)
        {
            _dialogService = dialogService;
            _navigationService = navigationService;
            _userService = userService;
            _quizQuestionService = quizQuestionService;
            _gameAssistantService = gameAssistantService;

            ClickFirstAnswerCommand = new RelayCommand(ClickFirstAnswer);
            ClickSecondAnswerCommand = new RelayCommand(ClickSecondAnswer);
            ClickThirdAnswerCommand = new RelayCommand(ClickThirdAnswer);
            ClickFourthAnswerCommand = new RelayCommand(ClickFourthAnswer);
            GoNextCommand = new RelayCommand(GoNext);
        }


        DispatcherTimer newTimer;
        public void Initialize()
        {
            IsDataLoaded = true;
            IsTaskbarVisible = true;

            QuizQuestion question = _quizQuestionService.GetRandomQuestion();
            QuizQuestion = question;

            _userService.ReloadAppUser();

            Visibility = System.Windows.Visibility.Collapsed;
            TicksToEnd = 100;


            // creating timer instance
            newTimer = new DispatcherTimer();
            // timer interval specified as 1 second
            newTimer.Interval = TimeSpan.FromSeconds(1);
            // Sub-routine OnTimerTick will be called at every 1 second
            newTimer.Tick += OnTimerTick;
            // starting the timer
            newTimer.Start();

            IsDataLoaded = false;
        }

        
        void OnTimerTick(Object sender, EventArgs args)
        {
            if (TicksToEnd <= 0)
            {
                if (Visibility == System.Windows.Visibility.Collapsed)
                {
                    ClickAnswer(0);
                    MessageToPlayer = "Skończył Ci się czas";
                }

                Visibility = System.Windows.Visibility.Visible;
                return;
            }

            
            TicksToEnd = TicksToEnd - 10;
        }


        private void GoNext()
        {
            CleanUp();
            _navigationService.Navigate<MarathonQuestionViewModel>(DateTime.Now.ToString());
        }

        private void ClickFourthAnswer()
        {
            ClickAnswer(4);
        }

        private void ClickThirdAnswer()
        {
            ClickAnswer(3);
        }

        private void ClickSecondAnswer()
        {
            ClickAnswer(2);
        }

        private void ClickFirstAnswer()
        {
            ClickAnswer(1);
        }


        private void ClickAnswer(int AnswerType)
        {
            if (TicksToEnd == 0)
                AnswerType = 0;

            //_quizQuestionService.MarkQuestionAsCompleted(QuizQuestion.QuizQuestionId);
            string answerText = "";
            switch (AnswerType)
            {
                case 1:
                    answerText = QuizQuestion.FirstAnswer;
                    break;
                case 2:
                    answerText = QuizQuestion.SecondAnswer;
                    break;
                case 3:
                    answerText = QuizQuestion.ThirdQuestion;
                    break;
                case 4:
                    answerText = QuizQuestion.FourthAnswer;
                    break;
                default:
                    break;
            }


            bool correctAnswer;
            if (answerText == QuizQuestion.CorrectAnswer)
            {
                correctAnswer = true;
                CorrectAnswerCount++;
                CorrectAnswerCountInRow++;
                MessageToPlayer = "Brawo! Zdobyłeś punkt";
                _gameAssistantService.AddEvent(3);

                if (CorrectAnswerCount == 10) {
                    _gameAssistantService.AddEvent(7);
                    _dialogService.ShowDialog("Zdobyłeś osiągnięcie 10 poprawnych odpowiedzi - otrzymujesz 10 punktów", "Gratulacje");
                }

                if (CorrectAnswerCount == 20)
                {
                    _gameAssistantService.AddEvent(8);
                    _dialogService.ShowDialog("Zdobyłeś osiągnięcie 20 poprawnych odpowiedzi - otrzymujesz 20 punktów", "Gratulacje");
                }

                if (CorrectAnswerCount == 50)
                {
                    _gameAssistantService.AddEvent(9);
                    _dialogService.ShowDialog("Zdobyłeś osiągnięcie 50 poprawnych odpowiedzi - otrzymujesz 50 punktów", "Gratulacje");
                }

                if (CorrectAnswerCount == 100)
                {
                    _gameAssistantService.AddEvent(10);
                    _dialogService.ShowDialog("Zdobyłeś osiągnięcie 100 poprawnych odpowiedzi - otrzymujesz 100 punktów", "Gratulacje");
                }



                if (CorrectAnswerCountInRow == 10)
                {
                    _gameAssistantService.AddEvent(11);
                    _dialogService.ShowDialog("Zdobyłeś osiągnięcie 10 poprawnych odpowiedzi pod rząd - otrzymujesz 20 punktów", "Gratulacje");
                }

                if (CorrectAnswerCountInRow == 20)
                {
                    _gameAssistantService.AddEvent(12);
                    _dialogService.ShowDialog("Zdobyłeś osiągnięcie 20 poprawnych odpowiedzi pod rząd - otrzymujesz 40 punktów", "Gratulacje");
                }

                if (CorrectAnswerCountInRow == 50)
                {
                    _gameAssistantService.AddEvent(13);
                    _dialogService.ShowDialog("Zdobyłeś osiągnięcie 50 poprawnych odpowiedzi pod rząd - otrzymujesz 100 punktów", "Gratulacje");
                }

                if (CorrectAnswerCountInRow == 100)
                {
                    _gameAssistantService.AddEvent(14);
                    _dialogService.ShowDialog("Zdobyłeś osiągnięcie 100 poprawnych odpowiedzi pod rząd - otrzymujesz 200 punktów", "Gratulacje");
                }
            }
            else
            {
                CorrectAnswerCountInRow = 0;
                correctAnswer = false;
                MessageToPlayer = "Niestety, to nie jest poprawna odpowiedź..";
                _gameAssistantService.AddEvent(6);
            }


            switch (AnswerType)
            {
                case 1:
                    if (correctAnswer)
                        FirstAnswerColor = "green";
                    else
                        FirstAnswerColor = "red";
                    break;
                case 2:
                    if (correctAnswer)
                        SecondAnswerColor = "green";
                    else
                        SecondAnswerColor = "red";
                    break;
                case 3:
                    if (correctAnswer)
                        ThirdAnswerColor = "green";
                    else
                        ThirdAnswerColor = "red";
                    break;
                case 4:
                    if (correctAnswer)
                        FourthAnswerColor = "green";
                    else
                        FourthAnswerColor = "red";
                    break;
                default:
                    FirstAnswerColor = "red";
                    SecondAnswerColor = "red";
                    ThirdAnswerColor = "red";
                    FourthAnswerColor = "red";
                    break;
            }

            if (QuizQuestion.FirstAnswer == QuizQuestion.CorrectAnswer)
                FirstAnswerColor = "green";
            if (QuizQuestion.SecondAnswer == QuizQuestion.CorrectAnswer)
                SecondAnswerColor = "green";
            if (QuizQuestion.ThirdQuestion == QuizQuestion.CorrectAnswer)
                ThirdAnswerColor = "green";
            if (QuizQuestion.FourthAnswer == QuizQuestion.CorrectAnswer)
                FourthAnswerColor = "green";

            TicksToEnd = 0;
            Visibility = System.Windows.Visibility.Visible;
            _userService.ReloadAppUser();
        }






        public void CleanUp()
        {
            if(newTimer != null)
                newTimer.Tick -= OnTimerTick;
            MessageToPlayer = null;
            TicksToEnd = 100;

            FirstAnswerColor = null;
            SecondAnswerColor = null;
            ThirdAnswerColor = null;
            FourthAnswerColor = null;

            IsDataLoaded = true;
            QuizQuestion = null;
        }

        internal void GoBack()
        {
            _navigationService.Navigate<WelcomeViewModel>();
        }
    }
}
