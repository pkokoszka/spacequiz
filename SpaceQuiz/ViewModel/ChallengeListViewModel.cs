﻿using GalaSoft.MvvmLight;
using SpaceQuiz.Contracts;
using SpaceQuiz.Model;
using System.Collections.ObjectModel;

namespace SpaceQuiz.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ChallengeListViewModel : ViewModelBase
    {
        private readonly ISpaceQuizService spaceQuizService;
        private readonly IUserService userService;

        private ObservableCollection<QuizChallenge> _QuizChallengeToDoList;
        public ObservableCollection<QuizChallenge> QuizChallengeToDoList
        {
            get { return _QuizChallengeToDoList; }
            set
            {
                if (_QuizChallengeToDoList != value)
                {
                    _QuizChallengeToDoList = value;
                    RaisePropertyChanged(() => QuizChallengeToDoList);
                }
            }
        }

        private ObservableCollection<QuizChallenge> _QuizChallengeDoneList;
        public ObservableCollection<QuizChallenge> QuizChallengeDoneList
        {
            get { return _QuizChallengeDoneList; }
            set
            {
                if (_QuizChallengeDoneList != value)
                {
                    _QuizChallengeDoneList = value;
                    RaisePropertyChanged(() => QuizChallengeDoneList);
                }
            }
        }


        public ChallengeListViewModel(ISpaceQuizService _spaceQuizService, IUserService _userService)
        {
            userService = _userService;
            spaceQuizService = _spaceQuizService;
        }



        internal void Initialize()
        {
            QuizChallengeDoneList = spaceQuizService.GetDoneChallenges();
            QuizChallengeToDoList = spaceQuizService.GetToDoChallenges();
        }
    }
}