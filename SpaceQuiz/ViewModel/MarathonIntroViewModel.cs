﻿using GalaSoft.MvvmLight.Command;
using SpaceQuiz.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceQuiz.ViewModel
{
    public class MarathonIntroViewModel
    {
        private INavigationService _navigationService;
        public RelayCommand GoToMarathonQuizCommand { get; private set; }


        public MarathonIntroViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;

            GoToMarathonQuizCommand = new RelayCommand(GoToMarathonQuiz);
        }

        private void GoToMarathonQuiz()
        {
            _navigationService.Navigate<MarathonQuestionViewModel>();
        }
    }
}
