﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Phone.Tasks;
using SpaceQuiz.Contracts;

namespace SpaceQuiz.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class AddQuestionViewModel : ViewModelBase
    {
        public RelayCommand SendCommand { get; private set; }

        private string content;
        public string Content
        {
            get { return content; }
            set
            {
                if (content != value)
                {
                    content = value;
                    RaisePropertyChanged(() => Content);
                }
            }
        }

        private string firstAnswer;
        public string FirstAnswer
        {
            get { return firstAnswer; }
            set
            {
                if (firstAnswer != value)
                {
                    firstAnswer = value;
                    RaisePropertyChanged(() => FirstAnswer);
                }
            }
        }

        private string secondAnswer;
        public string SecondAnswer
        {
            get { return secondAnswer; }
            set
            {
                if (secondAnswer != value)
                {
                    secondAnswer = value;
                    RaisePropertyChanged(() => SecondAnswer);
                }
            }
        }

        private string thirdAnswer;
        public string ThirdAnswer
        {
            get { return thirdAnswer; }
            set
            {
                if (thirdAnswer != value)
                {
                    thirdAnswer = value;
                    RaisePropertyChanged(() => ThirdAnswer);
                }
            }
        }

        private string fourthAnswer;
        public string FourthAnswer
        {
            get { return fourthAnswer; }
            set
            {
                if (fourthAnswer != value)
                {
                    fourthAnswer = value;
                    RaisePropertyChanged(() => FourthAnswer);
                }
            }
        }

        private readonly ISpaceQuizService _spaceQuizService;
        private readonly IGameAssistantService _gameAssistantService;
        private readonly IUserService _userService;

        public AddQuestionViewModel(ISpaceQuizService spaceQuizService, IUserService userService, IGameAssistantService gameAssistantService)
        {
            _userService = userService;
            _spaceQuizService = spaceQuizService;
            _gameAssistantService = gameAssistantService;
            SendCommand = new RelayCommand(SendQuestion);
        }

        internal void Initialize()
        {

        }

        public void SendQuestion() 
        {
            if (Content == null || FirstAnswer == null || SecondAnswer == null || ThirdAnswer == null || FourthAnswer == null)
                return;

            _gameAssistantService.AddEvent(226);
            _userService.ReloadAppUser();

            EmailComposeTask emailComposeTask = new EmailComposeTask();

            emailComposeTask.Subject = "SpaceQuiz: Wiadomość";
            emailComposeTask.Body = "[" + Content + "]" + "[" + FirstAnswer + "]" + "[" + SecondAnswer + "]" + "[" + ThirdAnswer + "]" + "[" + FourthAnswer + "]";
            emailComposeTask.To = "admin@kozacy.org";

            emailComposeTask.Show();

            
        }
    }
}