﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Phone.Info;
using Microsoft.Phone.Shell;
using SpaceQuiz.Contracts;
using SpaceQuiz.Helper.GameAssistant;
using SpaceQuiz.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Navigation;
using Windows.Phone.System.Analytics;

namespace SpaceQuiz.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class RegisterUserViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IDialogService _dialogService;
        private readonly IUserService _userService;
        private readonly IGameAssistantService _gameAssistantService;

        private bool isDataLoaded;
        private string name;
        private string loadingText;

        private bool isTaskbarVisible = true;
        private ObservableCollection<User> UserList;

        private User user;
        public RelayCommand CreateUserCommand { get; private set; }
        public RelayCommand CancelCommand { get; private set; }
        public bool IsDataLoaded
        {
            get { return isDataLoaded; }
            set
            {
                if (isDataLoaded != value)
                {
                    isDataLoaded = value;
                    RaisePropertyChanged(() => IsDataLoaded);
                }
            }
        }



        public string LoadingText
        {
            get { return loadingText; }
            set
            {
                if (loadingText != value)
                {
                    loadingText = value;
                    RaisePropertyChanged(() => LoadingText);
                }
            }
        }

        public bool IsTaskbarVisible
        {
            get { return isTaskbarVisible; }
            set
            {
                if (isTaskbarVisible != value)
                {
                    isTaskbarVisible = value;
                    RaisePropertyChanged(() => IsTaskbarVisible);
                }
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    name = value;
                    RaisePropertyChanged(() => Name);
                }
            }
        }



        public RegisterUserViewModel(IDialogService dialogService, INavigationService navigationService, IUserService userService, IGameAssistantService gameAssistantService)
        {
            _dialogService = dialogService;
            _navigationService = navigationService;
            _userService = userService;
            _gameAssistantService = gameAssistantService;

            CreateUserCommand = new RelayCommand(CreateUser);
            CancelCommand = new RelayCommand(NavigateToBack);
        }

        public async void Initialize()
        {
            IsDataLoaded = false;
            user = _navigationService.GetNavigationParameter() as User;
            if (user != null)
            {
                IsTaskbarVisible = false;
                _dialogService.ShowDialog("Jesteś już zarejestowany", "Błąd");
                IsTaskbarVisible = true;
                IsDataLoaded = true;
                return;
            }
            CreateUserCommand.RaiseCanExecuteChanged();

            UserList = await _userService.GetUsers();
            LoadingText = "Ładuję";
            IsDataLoaded = false;
        }

        public void CleanUp()
        {

        }

        private async void CreateUser()
        {
            IsDataLoaded = true;
            if (await ValidateParameters())
                return;

            
            var user = new User();
            user.UserName = name;
            user.ProjectId = GameAssistantConfig.ProjectId;
            user.SourceUserId = App.SourceUserId;
            
            LoadingText = "Tworzę użytkownika";

            var createdUser = await _userService.CreateUser(user);
            if (createdUser == null)
            {
                IsTaskbarVisible = false;
                _dialogService.ShowDialog("Wystąpił błąd", "Błąd");
                IsTaskbarVisible = false;
            }
            else
            {
                App.User = createdUser;
                IsTaskbarVisible = false;
                _dialogService.ShowDialog("Rejestracja przebiegła pomyślnie! Dziękujemy", "Gotowe");
                IsTaskbarVisible = true;
                MessengerInstance.Send(true, "UserRegisterToken");
                _navigationService.Navigate<WelcomeViewModel>(user.UserId.ToString());
            }

            _gameAssistantService.AddAchievement(6);
            ViewModelLocator.Cleanup();
            IsDataLoaded = false;
        }

        private void NavigateToBack()
        {
            CleanUp();
            //_navigationService.GoBack();
        }

        public async Task<bool> ValidateParameters()
        {
            if (string.IsNullOrEmpty(Name))
            {
                _dialogService.ShowDialog("Wpisz nazwę użytkownika", "Błąd");
                return true;
            }

            foreach (var item in UserList)
            {
                if (item.UserName == Name)
                {
                    _dialogService.ShowDialog("Istnieje taka nazwa użytkownika", "Błąd"); 
                    return true;
                }

            }

            return false;
        }




        //public void ShowMessage()
        //{
        //    System.Windows.MessageBox.Show(this.Name + "aaaaa");
        //    //string PostalCode = "aaa";
        //    //PhoneApplicationService.Current.State["DatatFromFirstPage"] = PostalCode;
        //    //_navigationService.NavigateTo("MvvmView", PostalCode);            
        //}

        ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}
    }
}