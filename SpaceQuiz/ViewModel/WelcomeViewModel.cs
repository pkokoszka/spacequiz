﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using SpaceQuiz.Contracts;
using SpaceQuiz.Model;
using System.Collections.ObjectModel;

namespace SpaceQuiz.ViewModel
{

    public class WelcomeViewModel : ViewModelBase
    {

        private readonly INavigationService _navigationService;
        private readonly IDialogService _dialogService;
        private readonly IUserService _userService;
        private readonly IGameAssistantService _gameAssistantService;

        private bool isDataLoaded;
        private string name;
        private string loadingText;

        private bool isTaskbarVisible = true;
        private ObservableCollection<User> UserList;

        private User user;
        public RelayCommand GoToChallengeCommand { get; private set; }
        public RelayCommand GoToMarathonCommand { get; private set; }
        public bool IsDataLoaded
        {
            get { return isDataLoaded; }
            set
            {
                if (isDataLoaded != value)
                {
                    isDataLoaded = value;
                    RaisePropertyChanged(() => IsDataLoaded);
                }
            }
        }






        public string LoadingText
        {
            get { return loadingText; }
            set
            {
                if (loadingText != value)
                {
                    loadingText = value;
                    RaisePropertyChanged(() => LoadingText);
                }
            }
        }

        public bool IsTaskbarVisible
        {
            get { return isTaskbarVisible; }
            set
            {
                if (isTaskbarVisible != value)
                {
                    isTaskbarVisible = value;
                    RaisePropertyChanged(() => IsTaskbarVisible);
                }
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    name = value;
                    RaisePropertyChanged(() => Name);
                }
            }
        }



        public WelcomeViewModel(IDialogService dialogService, INavigationService navigationService, IUserService userService, IGameAssistantService gameAssistantService)
        {
            _dialogService = dialogService;
            _navigationService = navigationService;
            _userService = userService;
            _gameAssistantService = gameAssistantService;

            GoToChallengeCommand = new RelayCommand(GoToChallenge);
            GoToMarathonCommand = new RelayCommand(GoToMarathon);
        }

        private void GoToChallenge()
        {
            _navigationService.Navigate<ChallengeListViewModel>();
        }

        public async void Initialize(string key)
        {
            IsDataLoaded = false;
            user = _navigationService.GetNavigationParameter() as User;
            if (user != null)
            {
                IsTaskbarVisible = false;
                _dialogService.ShowDialog("Jesteś już zarejestowany", "Błąd");
                IsTaskbarVisible = true;
                IsDataLoaded = true;
                return;
            }

            UserList = await _userService.GetUsers();

            _gameAssistantService.AddEvent(5);
            LoadingText = "Ładuję";
            IsDataLoaded = false;
        }

        public void CleanUp()
        {

        }

        private void GoToMarathon()
        {
            _navigationService.Navigate<MarathonIntroViewModel>();
        }


        private void NavigateToBack()
        {
            CleanUp();
            //_navigationService.GoBack();
        }

    }
}
