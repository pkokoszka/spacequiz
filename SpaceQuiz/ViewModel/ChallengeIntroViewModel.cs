﻿using GalaSoft.MvvmLight.Command;
using SpaceQuiz.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceQuiz.ViewModel
{
    public class ChallengeIntroViewModel
    {
        private INavigationService _navigationService;
        public RelayCommand GoToMarathonQuizCommand { get; private set; }

        public int QuizChallengeId { get; set; }

        public ChallengeIntroViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;

            GoToMarathonQuizCommand = new RelayCommand(GoToMarathonQuiz);
        }

        private void GoToMarathonQuiz()
        {
            _navigationService.Navigate<ChallengeQuestionViewModel>(QuizChallengeId.ToString());
        }

        internal void CleanUp()
        {
            QuizChallengeId = 0;
        }

        internal void Initialize(int QuizChallengeId)
        {
            this.QuizChallengeId = QuizChallengeId;
        }

        internal void Initialize()
        {

        }
    }
}
